ARG PROXY_CACHE

FROM ${PROXY_CACHE}golang:alpine3.18 AS build

WORKDIR /app

COPY . /app

RUN go build -o changelog .

FROM ${PROXY_CACHE}alpine:3.18

RUN apk add --no-cache curl=~8 git=~2 openssh=~9

WORKDIR /app

COPY --from=build /app/changelog /usr/local/bin/changelog

ENTRYPOINT ["/usr/local/bin/changelog"]
