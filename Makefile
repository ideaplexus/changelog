#!make

# Set Playbook based on environment
ifdef CI
PLAYBOOK := "ansible.ci.yml"
else
PLAYBOOK := "ansible.dev.yml"
endif

.PHONY: help
help: ## Show this help.
	@awk 'BEGIN {FS = ":.*##"; printf "Usage:\033[36m\033[0m\n"} /^[$$()% a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-17s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)

.PHONY: init
init: ## Download the playbook dependencies.
	 ansible-galaxy install -r ansible.requirements.yml

.PHONY: container
container: ## (Build and) start the containers.
	ansible-playbook $(PLAYBOOK) --tags "container-ssh,container-dev"

.PHONY: container-rebuild
container-rebuild: ## Force the rebuild of the containers and starts them.
	ansible-playbook $(PLAYBOOK) --tags "container-ssh:rebuild,container-dev:rebuild"

.PHONY: all
all: ## Run all targets.
	ansible-playbook $(PLAYBOOK)

.PHONY: clean
clean: ## Clean the project by removing vendor, build, lint and test output.
	ansible-playbook $(PLAYBOOK) --tags="clean"

.PHONY: check
check: ## Ensure code is checked with lint and test before commit.
	ansible-playbook $(PLAYBOOK) --tags="go:lint,go:test"

.PHONY: vendor
vendor: ## Setup the dependencies.
	ansible-playbook $(PLAYBOOK) --tags="go:vendor"

.PHONY: vendor-upgrade
vendor-upgrade: ## Upgrade the dependencies.
	ansible-playbook $(PLAYBOOK) --tags="go:vendor:upgrade"

.PHONY: generate
generate: ## Run all configured generate tasks.
	ansible-playbook $(PLAYBOOK) --tags="generate"

.PHONY: fix
fix: ## Lint the application and output in checkstyle format.
	ansible-playbook $(PLAYBOOK) --tags="go:fix"

.PHONY: lint
lint: ## Lint the application and output in checkstyle format.
	ansible-playbook $(PLAYBOOK) --tags="go:lint"

.PHONY: test
test: ## Test the application and generate coverage report files.
	ansible-playbook $(PLAYBOOK) --tags="go:test"

.PHONY: build
build: ## Build the binaries.
	ansible-playbook $(PLAYBOOK) --tags="go:build"
